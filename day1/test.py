# /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Unit tests to check that our results matches the examples"""

import unittest
from day1.floors import Instructions


class TestInstructions(unittest.TestCase):

    """Tests Instructions methods"""

    def setUp(self):
        """Initialize with the example input"""
        self.instructions = {
            '(())': 0,
            '()()': 0,
            '(((': 3,
            '(()(()(': 3,
            '))(((((': 3,
            '())': -1,
            '))(': -1,
            ')))': -3,
            ')())())': -3,
        }
        self.positions_basement = {
            ')': 1,
            '()())': 5,
        }

    def test_instructions_to_floor(self):
        """Test our navigation matches the given examples"""
        for instruction, floor in self.instructions.items():
            nav = Instructions(instruction)
            calculated_floor = nav.follow()
            self.assertEqual(floor, calculated_floor)

    def test_positions_to_basement(self):
        """Test our position counting matches the given examples"""
        for instruction, position in self.positions_basement.items():
            nav = Instructions(instruction)
            calculated_position = nav.to_floor(-1)
            self.assertEqual(position, calculated_position)


if __name__ == '__main__':
    unittest.main()
