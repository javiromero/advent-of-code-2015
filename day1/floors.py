# /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Day one solutions"""

import os


class Instructions(object):

    """Instructions to navigate a huge building's floors"""

    def __init__(self, instructions=None):
        if not instructions:
            base = os.path.abspath(os.path.dirname(__file__))
            filepath = os.path.join(base, 'input.txt')
            with open(filepath, 'r') as file_input:
                instructions = file_input.read()
        self.instructions = instructions

    def follow(self):
        """In what floor do we end if we follow the instructions"""
        up = self.instructions.count('(')
        down = self.instructions.count(')')
        return up - down

    def to_floor(self, floor):
        """How many instructions we must follow to end in the desired floor"""
        for position, char in enumerate(self.instructions, start=1):
            partial = self.instructions[:position]
            calculated_floor = partial.count('(') - partial.count(')')
            if calculated_floor == floor:
                return position
        return False

    def run(self):
        """Solve the test"""
        # Part one:
        final_floor = self.follow()
        print("The instructions take Santa to floor %(floor)s" % {
            'floor': final_floor,
        })

        # Part two:
        final_instruction = self.to_floor(-1)
        print("The first instruction to reach the basement is %(position)s" % {
            'position': final_instruction,
        })

if __name__ == '__main__':
    inst = Instructions()
    inst.run()
